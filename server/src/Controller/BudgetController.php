<?php

namespace App\Controller;

use App\Entity\Budget;
use App\Repository\BudgetRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;
use App\Entity\User;
use App\Repository\UserRepository;

/**
 * @Route("/api/budget", name="api_budget")
 */

class BudgetController extends Controller
{
    private $serializer;

    public function __construct()
    {
        $encoder = new JsonEncoder();
        $normalizer = new GetSetMethodNormalizer();
        $normalizer->setCircularReferenceLimit(1);
        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });
        $this->serializer = new Serializer([$normalizer], [$encoder]);
    }

    /**
     * @Route("/{id}", name="one_budget", methods="GET")
     */
    public function findById(Budget $budget) : Response
    {   
        $data = $this->serializer->normalize($budget, null, ['attributes' => ['id', 'name', 'sum', 'operations' => ['id', 'sum', 'description', 'tags', 'type', 'date', 'budgets']]]);
        $response = new Response($this->serializer->serialize($data, "json"));
        return $response;
    }

    /**
     * @Route("", name="all_budgets_by_user", methods={"GET"})
     */
    public function all(UserRepository $repo)
    {   $user = $this->getUser();
        $list = $repo->getBudgetsByUser($user);
        $data = $this->serializer->normalize($list, null, ['attributes' => ['id', 'name', 'sum', 'operations']]);

        $response = new Response($this->serializer->serialize($data, 'json'));
        return $response;
    }

    /**
     * @Route("", name="new_budget", methods={"POST"})
     */
    public function addBudget(Request $request) : Response
    {
        $manager = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $content = $request->getContent();
        $budget = $this->serializer->deserialize($content, Budget::class, "json");
        $budget->setUser($user);

        $manager->persist($budget);
        $manager->flush();

        $data = $this->serializer->normalize($budget, null, ['attributes' => ['id', 'name', 'sum', 'user_id']]);

        $response = new Response($this->serializer->serialize($budget, "json"));
        return $response;
    }

    /**
     * @Route("/{id}", name="update_budget", methods={"PUT"})
     */
    public function update(Request $request, Budget $budget)
    {

        $manager = $this->getDoctrine()->getManager();

        $content = $request->getContent();
        $update = $this->serializer->deserialize($content, Budget::class, "json");

        $budget->setName($update->getName());
        $budget->setSum($update->getSum());
        $budget->setSum($update->getSum());

        $manager->persist($budget);
        $manager->flush();

        $data = $this->serializer->normalize($budget, null, ['attributes' => ['id', 'name', 'sum', 'operations']]);

        $response = new Response($this->serializer->serialize($data, "json"));
        return $response;
    }

    /**
     * @Route("/{id}", name="delete_budget", methods={"DELETE"})
     */
    public function delete(Budget $budget)
    {
        $manager = $this->getDoctrine()->getManager();

        $manager->remove($budget);
        $manager->flush();

        return new Response("OK", 204);
    }

    /**
     * @Route("/{id}/total", name="total_budget", methods={"GET"})
     */
    public function getTotal(Budget $budget, BudgetRepository $repo)
    {
        $data = $repo->getTotal($budget);
        $response = new Response($this->serializer->serialize($data[0], 'json'));
        return $response;
    }

    /**
     * @Route("/{id}/total-input", name="total-input_budget", methods={"GET"})
     */
    public function getTotalInput(Budget $budget, BudgetRepository $repo)
    {
        $data = $repo->getTotalInput($budget);
        $response = new Response($this->serializer->serialize($data[0], 'json'));
        return $response;
    }

    /**
     * @Route("/{id}/total-output", name="total-output_budget", methods={"GET"})
     */
    public function getTotalOutput(Budget $budget, BudgetRepository $repo)
    {
        $data = $repo->getTotalOutput($budget);
        $response = new Response($this->serializer->serialize($data[0], 'json'));
        return $response;
    }
}
