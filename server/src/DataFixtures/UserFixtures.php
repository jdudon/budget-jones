<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\User;

class UserFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i=0; $i < 4; $i++) { 
            $user = new User();
            $user->setEmail("{$i}@jones.fr");
            $user->setPassword("{$i}password");
            $manager->persist($user);
        }

        $manager->flush();
    }
}
