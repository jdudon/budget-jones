<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BudgetRepository")
 */
class Budget
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="float")
     */
    private $sum;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Operation", inversedBy="budgets")
     */
    private $operations;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="budgets")
     */
    private $user;

    public function __construct()
    {
        $this->operations = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName() : ? string
    {
        return $this->name;
    }

    public function setName(string $name) : self
    {
        $this->name = $name;

        return $this;
    }

    public function getSum() : ? float
    {
        return $this->sum;
    }

    public function setSum(float $sum) : self
    {
        $this->sum = $sum;

        return $this;
    }

    /**
     * @return Collection|Operation[]
     */
    public function getOperations() : Collection
    {
        return $this->operations;
    }

    public function addOperation(Operation $operation) : self
    {
        if (!$this->operations->contains($operation)) {
            $this->operations[] = $operation;
        }

        return $this;
    }

    public function removeOperation(Operation $operation) : self
    {
        if ($this->operations->contains($operation)) {
            $this->operations->removeElement($operation);
        }

        return $this;
    }

    public function getUser() : ? User
    {
        return $this->user;
    }

    public function setUser(? User $user) : self
    {
        $this->user = $user;

        return $this;
    }

    public function getTotal() : float
    {
        $somme = 0;
        foreach ($this->operations as $operation) {
            $somme += $operation->getSum();
        }
        return $somme;
    }

    public function getTotalInput() : float
    {
        $somme = 0;
        foreach ($this->operations as $operation) {
            if($operation->getSum()>0){
                $somme += $operation->getSum();
            }
        }
        return $somme;
    }
    public function getTotalOutput() : float
    {
        $somme = 0;
        foreach ($this->operations as $operation) {
            if($operation->getSum()<0){
                $somme += $operation->getSum();
            }
        }
        return $somme;
    }
}
